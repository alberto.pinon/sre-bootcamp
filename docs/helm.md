!!! info "Minikube in MacOS 🔥"

      In order to fix the "Ingress not supported" issue, please follow the steps below:
      ```
      # delete your current installation
      minikube delete
      # instead of running minikube in docker, run it in a VM (hyperkit)
      minikube start --driver=hyperkit
      ```

## Helm

```
helm create poke-chart
```


## MSSQL in Helm

we can import charts from another repository, in this case our repository "kontinu".

```
helm repo add kontinu https://kontinu.github.io/charts/

helm repo update

helm upgrade --install mssql kontinu/mssql

```


## Poke App in Helm

To test our "code"

```
helm lint poke-chart

# render templates, to check if they are valid, valid indentation, etc.
helm template poke-chart
```


!!! Note

    Update:

    - values.yaml (image and use `registry.gitlab.com/kontinu/sre-bootcamp/master/app`)
    - templates/deployment.yaml (`containerPort: 5000`)



---
### configMap


Add configuration to the app to connect to the database. Open templates/deployment.yaml (Line 34)

```yaml
envFrom:
  - configMapRef:
      name: my-config
```

so the ending section should look like this:

```yaml
      containers:
        - name: {{ .Chart.Name }}
          envFrom:
            - configMapRef:
                name: my-config
          securityContext:
```


in values.yaml at the end add the following in order to pass the configuration to the app.

```yaml
config:
  DB_HOST: mssql
  DB_USER: sa
  #! No usar texto plano, sino mejor secrets.
  DB_PASSWORD: th3Passw0rd
  VERSION: "102"

```

Add a new file templates/configmap.yaml with the following content, it will read the values and inject them into the configmap.

```yaml

apiVersion: v1
kind: ConfigMap
metadata:
  name: my-config
data:
  {{ toYaml .Values.config |nindent 2}}

```



### Install and run

```
helm upgrade --install poke-chart poke-chart
```

List installed charts in all Namespaces

```
helm list -A
```


### Uninstall


```
helm uninstall poke-chart
helm uninstall mssql
```

---

# Helm dependencies

We can add dependencies as part of our charts (like "all batteries included")

in Chart.yaml add:

```yaml
dependencies:
  - name: mssql
    version: 0.0.2
    repository: https://kontinu.github.io/charts/
```

Update dependencies and install the "WHOLE system"

```
helm dep up poke-chart

helm upgrade --install poke-chart poke-chart
```
