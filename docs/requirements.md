# Installation requirements

Please install the following software before or during the bootcamp!

# Software

- [x] [vscode](https://code.visualstudio.com/download)
- [x] [git](https://git-scm.com/)
- [x] [Docker](https://www.docker.com/)

- [x] [Docker Desktop](https://www.docker.com/get-started/)
- [x] [Docker Compose](https://docs.docker.com/compose/install/)


- [x] [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [x] [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [x] [Helm](https://helm.sh/docs/intro/quickstart/)

- [x] [Lens](https://k8slens.dev/)
- [x] [k9s](https://github.com/derailed/k9s)

!!! info
    K9s or Lens or both

# IaC

- [x] [Terraform](https://www.terraform.io/)
- [x] [Packer](https://www.packer.io/)

- [x] [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
- [x] [Gcloud](https://cloud.google.com/sdk/docs/install)



# Accounts

- [x] [Create a Gitlab Account](https://gitlab.com/)
- [x] [Create a Github Account for vscode liveshare](https://github.com/)
